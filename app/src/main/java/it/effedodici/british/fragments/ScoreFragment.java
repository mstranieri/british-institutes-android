package it.effedodici.british.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.effedodici.british.R;
import it.effedodici.british.adapter.ChallangesAdapter;
import it.effedodici.british.adapter.ScoreAdapter;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.rest.UsersResponse;
import it.effedodici.british.utils.Constants;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 30/08/16.
 */
public class ScoreFragment extends ToolBarFragment {

    private RecyclerView mRecyclerView;
    private ScoreAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.score_fragment, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.score_users_list);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ScoreAdapter();
        mRecyclerView.setAdapter(mAdapter);

        setUpToolBar(root, getString(R.string.score_title));
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        showActivityProgress();
        Call<UsersResponse> usersCall = RetrofitManager.getApiService(Constants.BASE_URL).getUsersByScore();
        usersCall.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Response<UsersResponse> response, Retrofit retrofit) {
                if(response != null && response.isSuccess()) {
                    mAdapter.swapData(response.body().getUsers());
                }
                hideActivityProgress();
            }

            @Override
            public void onFailure(Throwable t) {
                showGeneralErroDialog();
                hideActivityProgress();
            }
        });
    }
}
