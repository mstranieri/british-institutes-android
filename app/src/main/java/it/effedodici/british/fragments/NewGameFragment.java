package it.effedodici.british.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.effedodici.british.R;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.RandomOpponentResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 30/08/16.
 */
public class NewGameFragment extends ToolBarFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.new_game_fragment, container, false);

        root.findViewById(R.id.new_game_random_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Find a random opponent
                showActivityProgress();
                Call<RandomOpponentResponse> randomCall = RetrofitManager.getApiService(Constants.BASE_URL).randomOpponent(Utils.getUserFromDefaults(getActivity()).getId());
                randomCall.enqueue(new Callback<RandomOpponentResponse>() {
                    @Override
                    public void onResponse(Response<RandomOpponentResponse> response, Retrofit retrofit) {
                        if(response !=null && response.isSuccess()) {
                            User opponent = response.body().getUser();
                            OpponentDescriptionFragment fragment = new OpponentDescriptionFragment();
                            fragment.setOpponent(opponent);
                            requestFragmentReplacement(fragment, null, true);
                            hideActivityProgress();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        showGeneralErroDialog();
                    }
                });
            }
        });

        root.findViewById(R.id.new_game_play_with_friends_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Play with friends
                FriendsListFragment fragm = new FriendsListFragment();
                requestFragmentReplacement(fragm, null, true);
            }
        });

        setUpToolBar(root, getString(R.string.new_game_title));
        return root;

    }
}
