package it.effedodici.british.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mirkopinna on 11/11/16.
 */
public class Challange {

    @SerializedName("_id")
    private String id;
    private User starter;
    private User challanger;
    @SerializedName("challanger_accepted")
    private boolean challangerAccepted;
    @SerializedName("next_move")
    private String nextMove;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("starter_state")
    private GameState starterState;
    @SerializedName("challanger_state")
    private GameState challangerState;
    private boolean terminated;


    public User getChallanger() {
        return challanger;
    }

    public void setChallanger(User challanger) {
        this.challanger = challanger;
    }

    public User getStarter() {
        return starter;
    }

    public void setStarter(User starter) {
        this.starter = starter;
    }

    public boolean isChallangerAccepted() {
        return challangerAccepted;
    }

    public void setChallangerAccepted(boolean challangerAccepted) {
        this.challangerAccepted = challangerAccepted;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNextMove() {
        return nextMove;
    }

    public void setNextMove(String nextMove) {
        this.nextMove = nextMove;
    }

    public GameState getChallangerState() {
        return challangerState;
    }

    public void setChallangerState(GameState challangerState) {
        this.challangerState = challangerState;
    }

    public GameState getStarterState() {
        return starterState;
    }

    public void setStarterState(GameState starterState) {
        this.starterState = starterState;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }
}
