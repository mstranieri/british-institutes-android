package it.effedodici.british.rest;

import it.effedodici.british.models.Challange;
import it.effedodici.british.models.Question;

/**
 * Created by mirkopinna on 15/11/16.
 */
public class SelectCategoryResponse {
    private Challange challange;
    private Question question;

    public Challange getChallange() {
        return challange;
    }

    public void setChallange(Challange challange) {
        this.challange = challange;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
