package it.effedodici.british.fragments;

import it.effedodici.british.BaseActivity;
import it.effedodici.british.R;
import it.effedodici.british.utils.YesNoButtonDialog;

/**
 * Created by mirkopinna on 17/11/16.
 */
public class GameFragment extends BaseFragment {

    @Override
    public boolean allowBackPress() {
        return false;
    }

    @Override
    public String getFragmentTag() {
        return null;
    }
}
