package it.effedodici.british.rest;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by mirkopinna on 09/11/16.
 */
public interface ApiService {

    @POST("/auth/login")
    @FormUrlEncoded
    Call<LoginResponse> login(@Field("email") String email, @Field("password") String password);

    @POST("/auth/fblogin")
    @FormUrlEncoded
    Call<FBLoginResponse> FBLogin(@Field("token") String token);

    @POST("/auth/register")
    @FormUrlEncoded
    Call<RegistrationResponse> register(@Field("email") String email, @Field("username") String username, @Field("password") String password, @Field("picture") String picturUrl, @Field("fcm_token") String fcmToken);

    @POST("/auth/fbregister")
    @FormUrlEncoded
    Call<FBRegistrationResponse> registerFB(@Field("username") String username, @Field("fb_token") String fbToken, @Field("picture_url") String pictureUrl, @Field("fcm_token") String fcmToken);

    @POST("/randomopponent")
    @FormUrlEncoded
    Call<RandomOpponentResponse> randomOpponent(@Field("user_id") String userId);

    @POST("/getusers")
    @FormUrlEncoded
    Call<UsersResponse> getUsers(@Field("user_id") String myId);

    @POST("/startchallange")
    @FormUrlEncoded
    Call<StartChallangeResponse> startChallange(@Field("starter_id") String starterId, @Field("challanger_id") String challangerId);

    @POST("/selectcategory")
    @FormUrlEncoded
    Call<SelectCategoryResponse> selectCategory(@Field("user_id") String userId, @Field("challange_id") String challangeId, @Field("category") String category);

    @POST("/answer")
    @FormUrlEncoded
    Call<AnswerResponse> answer(@Field("answer_right") String isRight, @Field("challange_id") String challangeId, @Field("user_id") String userId, @Field("points") int points);

    @POST("/user/challanges")
    @FormUrlEncoded
    Call<UserChallangesResponse> userChallanges(@Field("user_id") String userId);

    @POST("/getchallange")
    @FormUrlEncoded
    Call<ChallangeResponse> getChallange(@Field("challange_id") String challangeId);

    @POST("/notificationToken")
    @FormUrlEncoded
    Call<RegistrationResponse> updateFCMToken(@Field("user_id") String userId, @Field("token") String FCMToken);

    @retrofit2.http.POST("/profile/picture")
    @retrofit2.http.Multipart
    retrofit2.Call<PictureResponse> uploadPicture(@retrofit2.http.Part MultipartBody.Part file, @retrofit2.http.Part("user_id") RequestBody userId);

    @GET("/getusers/score")
        Call<UsersResponse> getUsersByScore();


}
