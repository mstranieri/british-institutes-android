package it.effedodici.british.utils;

/**
 * Created by mirkopinna on 09/11/16.
 */
public class Constants {

    //URLs
    public static final String BASE_URL = "http://api-bi.effedodici.it:3000/";
    //public static final String BASE_URL = "http://10.0.2.2:3000/";

    //USER
    public static final String ID_PREF_KEY = "it.effedodici.british.user_id";
    public static final String USERNAME_PREF_KEY = "it.effedodici.british.username_pref_key";
    public static final String EMAIL_PREF_KEY = "it.effedodici.british.email_pref_key";
    public static final String CREATED_PREF_KEY = "it.effedodici.british.created_pref_key";
    public static final String PICTURE_URL_PREF_KEY = "it.effedodici.british.picture_url_pref_key";
    public static final String POINTS_PREF_KEY = "it.effedodici.british.points_pref_key";
    public static final String FB_TOKEN = "it.effedodici.british.facebook_token";
    public static final String LOGIN_TYPE = "it.effedodici.british.login_type";
    public static final String FCM_TOKEN_PREF_KEY = "it.effedodici.british.fcm_token_pref_key";

    //CATEGORIES
    public static final String SCIENCE = "SCIENCE";
    public static final String HISTORY = "HISTORY";
    public static final String FOOD = "FOOD";
    public static final String TV_CINEMA = "TV_CINEMA";
    public static final String GOSSIP = "GOSSIP";
    public static final String MUSIC = "MUSIC";
    public static final String SPORT = "SPORT";
    public static final String GEOGRAPHY = "GEOGRAPHY";
    public static final String CULTURE = "CULTURE";

    //NOTIFICATIONS
    public static final String STATUS = "status";
    public static final String CHALLANGE_ID = "challange_id";
    public static final String CHALLANGE_STARTED = "CHALLANGE_STARTED";
    public static final String OPPONENT_RESPONDED = "OPPONENT_RESPONDED";
    public static final String CHALLENGE_NOTIFICATIONS = "challange_notifications";
    public static final String NOTIFICATION_ON = "it.effedodici.british.notificarion_on";

}
