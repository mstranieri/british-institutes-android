package it.effedodici.british.services;

/**
 * Created by mirkopinna on 03/02/17.
 */

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import it.effedodici.british.rest.RegistrationResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.rest.UsersResponse;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!TextUtils.isEmpty(preferences.getString(Constants.ID_PREF_KEY, ""))) {
            Call<RegistrationResponse> tokenCall = RetrofitManager.getApiService(Constants.BASE_URL).updateFCMToken(Utils.getUserFromDefaults(getApplicationContext()).getId(), token);
            tokenCall.enqueue(new Callback<RegistrationResponse>() {
                @Override
                public void onResponse(Response<RegistrationResponse> response, Retrofit retrofit) {
                    if(response.isSuccess()) {
                        Utils.saveUserToPreferences(getApplicationContext(), response.body().getUser());
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }
}
