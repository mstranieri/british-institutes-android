package it.effedodici.british;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ProgressBar;

import it.effedodici.british.fragments.BaseFragment;

/**
 * Created by mirkopinna on 09/11/16.
 */
public abstract class BaseActivity extends FragmentActivity {

    protected boolean onSaveInstanceCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onSaveInstanceCalled = false;

        /*getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });*/
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        onSaveInstanceCalled = true;

    }

    public void addFragment(BaseFragment fragment, String tag, boolean addToBackStack){
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.add(getFragmentContainerId(), fragment, tag);

        if(addToBackStack)
            ft.addToBackStack(tag);

        //if(!onSaveInstanceCalled) {
            ft.commit();
        //}
    }

    public void replaceFragment(BaseFragment fragment, String tag, boolean addToBackStack){
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.replace(getFragmentContainerId(), fragment, tag);

        if(addToBackStack)
            ft.addToBackStack(tag);

        //if(!onSaveInstanceCalled) {
            ft.commit();
        //}
    }


    public boolean popBackStack(){
        boolean isPop = false;
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            this.getSupportFragmentManager().popBackStackImmediate();
            isPop = true;
        }
        return isPop;
    }

    public void clearBackStack(String tag){
        this.getSupportFragmentManager().popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void showProgress() {
        if(getProgressBar() != null) {
            getProgressBar().setVisibility(View.VISIBLE);
        }
    }

    public void hideProgress() {
        if(getProgressBar() != null) {
            getProgressBar().setVisibility(View.GONE);
        }
    }

    public void replaceFragmentRequest(BaseFragment fragment, String tag, boolean addToBackStack){
        replaceFragment(fragment, tag, addToBackStack);
    }

    @Override
    public void onBackPressed() {
        BaseFragment fragm = (BaseFragment)getSupportFragmentManager().findFragmentById(getFragmentContainerId());
        if(fragm != null && fragm.getTag() != null && fragm.getTag().equals(getBaseFragmentTag())) {
            finish();
        } else if(fragm != null && !fragm.allowBackPress()){
            fragm.actionOnBackPress();
        }else {
            super.onBackPressed();
        }
    }

    public abstract int getFragmentContainerId();
    public abstract ProgressBar getProgressBar();
    public abstract String getBaseFragmentTag();

}
