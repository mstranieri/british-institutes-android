package it.effedodici.british.fragments;


import android.support.v4.app.Fragment;

import it.effedodici.british.BaseActivity;
import it.effedodici.british.R;
import it.effedodici.british.utils.SingleButtonDialog;

/**
 * Created by mirkopinna on 25/08/16.
 */
public abstract class BaseFragment extends Fragment {

    public void showGeneralErroDialog(){
        SingleButtonDialog error = new SingleButtonDialog();
        error.setMessage(getString(R.string.general_error));
        error.show(getChildFragmentManager(), "error");
    }

    public void requestFragmentReplacement(BaseFragment fragment, String tag, boolean keepToBack) {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).replaceFragmentRequest(fragment, tag, keepToBack);
        }
    }

    public void requestFragmentAdd(BaseFragment fragment, String tag, boolean keepToBack) {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).addFragment(fragment, tag, keepToBack);
        }
    }

    public void showActivityProgress() {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).showProgress();
        }
    }

    public void hideActivityProgress() {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).hideProgress();
        }
    }

    public void requestGoBack() {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).onBackPressed();
        }
    }

    public void requestGoHome() {
        requestClearBackStack(null);
        requestFragmentReplacement(new HomeFragment(), HomeFragment.TAG, false);
    }

    protected void requestClearBackStack(String tag) {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).clearBackStack(tag);
        }
    }

    public boolean allowBackPress() {
        return true;
    }

    public void actionOnBackPress() {
        return;
    }

    public abstract String getFragmentTag();

}
