package it.effedodici.british.rest;

import it.effedodici.british.models.FBUser;
import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 02/12/16.
 */
public class FBLoginResponse {

    private boolean logged;
    private FBUser user;

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    public FBUser getUser() {
        return user;
    }

    public void setUser(FBUser user) {
        this.user = user;
    }
}
