package it.effedodici.british.models;


import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import it.effedodici.british.utils.Constants;

/**
 * Created by mirkopinna on 09/11/16.
 */
public class User {

    public enum UserType {
        FACEBOOK,
        CLASSIC,
        UNDEFINED
    }

    @SerializedName("_id")
    private String id;
    private String username;
    private String password;
    private String email;
    private String picture_url;
    @SerializedName("created_at")
    private String created;
    private int points;
    private UserType type;
    @SerializedName("notification_token")
    private String FCMToken;
    //protected String fbToken;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture_url() {
        if(!TextUtils.isEmpty(picture_url) && !picture_url.startsWith("http") && !picture_url.startsWith("https")) {
            return Constants.BASE_URL + picture_url;
        } else {
            return picture_url;
        }

    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getFCMToken() {
        return FCMToken;
    }

    public void setFCMToken(String FCMToken) {
        this.FCMToken = FCMToken;
    }
}
