package it.effedodici.british.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.effedodici.british.R;
import it.effedodici.british.models.Challange;
import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 07/12/16.
 */
public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder>{

    private Context context;

    private ArrayList<User> users = new ArrayList<>();

    private ScoreAdapterListener listener;

    public interface ScoreAdapterListener {
        void onItemClicked(User user);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.score_user_row, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        User user = users.get(position);
        if(user != null) {
            holder.points.setText(context.getString(R.string.points, user.getPoints()));
            holder.position.setText(String.valueOf(position+1) + "°");
            if(position == 0) {
                holder.position.setTextColor(context.getResources().getColor(R.color.gold));
            } else if(position == 1) {
                holder.position.setTextColor(context.getResources().getColor(R.color.grey));
            } else if(position == 2) {
                holder.position.setTextColor(context.getResources().getColor(R.color.amaranto));
            } else {
                holder.position.setTextColor(Color.BLACK);
            }
            holder.name.setText(user.getUsername());
            if(!TextUtils.isEmpty(user.getPicture_url())) {
                Picasso.with(context).load(user.getPicture_url()).into(holder.picture);
            }
        }

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView position;
        public TextView name;
        public TextView points;
        public ImageView picture;
        public View container;
        public ViewHolder(View v) {
            super(v);
            position = (TextView) v.findViewById(R.id.score_row_position);
            picture = (ImageView) v.findViewById(R.id.score_row_picture);
            points = (TextView) v.findViewById(R.id.score_row_points);
            name = (TextView) v.findViewById(R.id.score_row_name);
            container = v.findViewById(R.id.score_main_content);
        }
    }

    public void swapData(List<User> users) {
        if(users != null) {
            this.users.clear();
            this.users.addAll(users);
            notifyDataSetChanged();
        }
    }

    public void setListener(ScoreAdapterListener listener) {
        this.listener = listener;
    }
}
