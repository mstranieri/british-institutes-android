package it.effedodici.british.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;

import it.effedodici.british.LoginActivity;
import it.effedodici.british.R;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;

/**
 * Created by mirkopinna on 10/11/16.
 */
public class SettingsFragment extends ToolBarFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.settings_fragment, container, false);
        setUpToolBar(root, getString(R.string.settings_title));

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        root.findViewById(R.id.settings_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.deleteUser(getActivity());
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                }
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });

        root.findViewById(R.id.settings_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFragmentReplacement(new ProfileFragment(), null, true);
            }
        });


        Switch notSwitch = (Switch)root.findViewById(R.id.settings_noifications_switch);
        notSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.edit().putBoolean(Constants.NOTIFICATION_ON, isChecked).apply();
            }
        });

        boolean isNotOn = preferences.getBoolean(Constants.NOTIFICATION_ON, true);
        notSwitch.setChecked(isNotOn);

        return root;
    }
}
