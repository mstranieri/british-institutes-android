package it.effedodici.british.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.effedodici.british.R;
import it.effedodici.british.models.Challange;
import it.effedodici.british.models.User;
import it.effedodici.british.utils.Utils;

/**
 * Created by mirkopinna on 16/11/16.
 */
public class ChallangesAdapter extends RecyclerView.Adapter<ChallangesAdapter.ViewHolder>{

    public interface ChallangesAdapterListener {
        void onItemClicked(Challange challnge);
    }

    private List<Challange> challanges = new ArrayList<>();
    private Context context;

    private ChallangesAdapterListener listener;

    public ChallangesAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.challanges_row, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(challanges != null && challanges.size() > 0) {
            final Challange challange = challanges.get(position);
            if(challange != null) {

                User challanger = null;
                if(Utils.getUserFromDefaults(context).getId().equals(challange.getChallanger().getId())) {
                    challanger = challange.getStarter();
                } else {
                    challanger = challange.getChallanger();
                }

                holder.title.setText(challanger.getUsername());

                if(!TextUtils.isEmpty(challange.getChallanger().getPicture_url())) {
                    Picasso.with(context).load(challanger.getPicture_url()).into(holder.picture);
                }

                holder.container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null) {
                            listener.onItemClicked(challange);
                        }
                    }
                });
                if(Utils.getUserFromDefaults(context).getId().equals(challange.getNextMove())){
                    holder.description.setText(context.getString(R.string.your_tourn));
                } else {
                    holder.description.setText(context.getString(R.string.opponent_turn_short));
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if(challanges != null) {
            return challanges.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView description;
        public ImageView picture;
        public View container;
        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.challanges_row_title);
            picture = (ImageView) v.findViewById(R.id.challanges_row_picture);
            description = (TextView) v.findViewById(R.id.challanges_row_desc);
            container = v.findViewById(R.id.challanges_row_container);
        }
    }

    public void swapData(List<Challange> challanges) {
        if(challanges != null) {
            this.challanges.clear();
            this.challanges.addAll(challanges);
            notifyDataSetChanged();
        }
    }

    public ChallangesAdapterListener getListener() {
        return listener;
    }

    public void setListener(ChallangesAdapterListener listener) {
        this.listener = listener;
    }
}
