package it.effedodici.british.fragments;

import android.view.View;
import android.widget.TextView;

import it.effedodici.british.R;

/**
 * Created by mirkopinna on 10/11/16.
 */
public class ToolBarFragment extends BaseFragment {

    public void setUpToolBar(View root, String title) {

        ((TextView)root.findViewById(R.id.custom_action_bar_title)).setText(title);

        root.findViewById(R.id.custom_action_bar_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestGoBack();
            }
        });
        
    }

    @Override
    public String getFragmentTag() {
        return null;
    }
}
