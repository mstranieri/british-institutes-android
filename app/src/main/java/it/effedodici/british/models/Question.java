package it.effedodici.british.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mirkopinna on 15/11/16.
 */
public class Question {

    private String question;
    private ArrayList<String> answers;
    private int correct;
    private int points;

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
