package it.effedodici.british.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.effedodici.british.R;

/**
 * Created by mirkopinna on 10/11/16.
 */
public class TutorialFragment extends ToolBarFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.tutorial_fragment, container, false);
        setUpToolBar(root, getString(R.string.tutorial_title));
        return root;
    }
}
