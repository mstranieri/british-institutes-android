package it.effedodici.british.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.squareup.picasso.Picasso;

import it.effedodici.british.R;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.ChallangeResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import it.effedodici.british.utils.YesNoButtonDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 25/08/16.
 */
public class HomeFragment extends BaseFragment {

    public static final String TAG = "Home";

    private User user;

    private ImageView profilePic;
    private TextView usernameLbl;
    private TextView pointsLbl;
    private TextView homeNotificationLbl;

    private BroadcastReceiver opponentActionReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.home_fragment, container, false);

        profilePic = (ImageView) root.findViewById(R.id.home_profile_picture);
        usernameLbl = (TextView) root.findViewById(R.id.home_username_lbl);
        pointsLbl = (TextView) root.findViewById(R.id.home_points_lbl);

        homeNotificationLbl = (TextView) root.findViewById(R.id.home_notification_badge);

        root.findViewById(R.id.home_share_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://www.britishinstitutes.it/");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        root.findViewById(R.id.home_challanges_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show challanges
                requestFragmentReplacement(new UserChallangesFragment(), HomeFragment.TAG, true);
            }
        });

        root.findViewById(R.id.home_new_game_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start new game
                requestFragmentReplacement(new NewGameFragment(), HomeFragment.TAG, true);
            }
        });

        root.findViewById(R.id.home_how_to_play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show tutorial
                requestFragmentReplacement(new TutorialFragment(), HomeFragment.TAG, true);
            }
        });

        root.findViewById(R.id.home_profile_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show profile
                ProfileFragment fragm = new ProfileFragment();
                fragm.setUser(user);
                requestFragmentReplacement(fragm, HomeFragment.TAG, true);
            }
        });

        root.findViewById(R.id.home_score_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show scores
                requestFragmentReplacement(new ScoreFragment(), HomeFragment.TAG, true);
            }
        });

        root.findViewById(R.id.home_settings_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show settings
                requestFragmentReplacement(new SettingsFragment(), HomeFragment.TAG, true);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        user = Utils.getUserFromDefaults(getActivity());
        fillUserInfos();

        opponentActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, final Intent intent) {

                YesNoButtonDialog challengeDialog = new YesNoButtonDialog();
                challengeDialog.setTitle("New Challenge");
                String status = intent.getStringExtra(Constants.STATUS);

                if(status.equals(Constants.CHALLANGE_STARTED)) {
                    challengeDialog.setMessage("Somebody wants to challenge you, would you like to play?");
                } else if(status.equals(Constants.OPPONENT_RESPONDED)) {
                    challengeDialog.setMessage("It\'s your turn, would you like to play?");
                }

                challengeDialog.setListener(new YesNoButtonDialog.YesNoButtonListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        String challangeId = intent.getStringExtra("challange_id");
                        Call<ChallangeResponse> challangeCall = RetrofitManager.getApiService(Constants.BASE_URL).getChallange(challangeId);
                        challangeCall.enqueue(new Callback<ChallangeResponse>() {
                            @Override
                            public void onResponse(Response<ChallangeResponse> response, Retrofit retrofit) {
                                if(response.isSuccess()) {
                                    CategoriesFragment fragment = new CategoriesFragment();
                                    fragment.setChallange(response.body().getChallange());
                                    requestFragmentReplacement(fragment, HomeFragment.TAG, true);
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                showGeneralErroDialog();
                            }
                        });
                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                });

                challengeDialog.show(getChildFragmentManager(), "challenge");

                fillUserInfos();
            }
        };

        IntentFilter challangeFilter = new IntentFilter(Constants.OPPONENT_RESPONDED);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(opponentActionReceiver, challangeFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(opponentActionReceiver);
    }

    private void fillUserInfos() {
        if(user != null) {

            String pictureUrl = user.getPicture_url();
            if(!TextUtils.isEmpty(pictureUrl)) {
                Picasso.with(getActivity()).load(pictureUrl).into(profilePic);
            }

            usernameLbl.setText(user.getUsername());
            pointsLbl.setText(String.valueOf(user.getPoints()) + " Points");

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            int notifications = preferences.getInt(Constants.CHALLENGE_NOTIFICATIONS, 0);
            if(notifications > 0) {
                homeNotificationLbl.setText(String.valueOf(notifications));
                homeNotificationLbl.setVisibility(View.VISIBLE);
            } else {
                homeNotificationLbl.setVisibility(View.GONE);
            }

        }
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
