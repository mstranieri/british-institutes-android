package it.effedodici.british;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import it.effedodici.british.fragments.LoginFragment;
import it.effedodici.british.models.User;
import it.effedodici.british.utils.Utils;

/**
 * Created by mirkopinna on 19/01/17.
 */
public class SplashActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        User user = Utils.getUserFromDefaults(this);
        if(!TextUtils.isEmpty(user.getId())) {

            boolean startNormal = true;

            //User already did login go to home.
            Intent startIntent = getIntent();
            if(startIntent != null) {
                String challangeId = startIntent.getStringExtra("challange_id");
                if(!TextUtils.isEmpty(challangeId)) {
                    //App started by a push notification
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("challange_id", challangeId);
                    startActivity(intent);
                    startNormal = false;
                }
            }

            if(startNormal) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }

        } else {
            //User needs to login
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

}
