package it.effedodici.british.rest;

/**
 * Created by mirkopinna on 05/12/16.
 */
public class PictureResponse {

    private String picture_url;

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }
}
