package it.effedodici.british.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.List;

import it.effedodici.british.R;
import it.effedodici.british.adapter.FriendsAdapter;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.rest.UsersResponse;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 22/11/16.
 */
public class FriendsListFragment extends ToolBarFragment{

    private GridView friendsGrid;

    private List<User> users;
    private FriendsAdapter friendsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.friends_list_fragment, container, false);
        friendsGrid = (GridView) root.findViewById(R.id.friends_grid_view);
        friendsAdapter = new FriendsAdapter();
        friendsGrid.setAdapter(friendsAdapter);
        friendsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = users.get(position);
                if(user != null) {
                    OpponentDescriptionFragment fragment = new OpponentDescriptionFragment();
                    fragment.setOpponent(user);
                    requestFragmentReplacement(fragment, null, true);
                }
            }
        });
        setUpToolBar(root, getString(R.string.play_with_friends));
        return root;

    }

    @Override
    public void onResume() {
        super.onResume();
        showActivityProgress();
        Call<UsersResponse> usersCall = RetrofitManager.getApiService(Constants.BASE_URL).getUsers(Utils.getUserFromDefaults(getActivity()).getId());
        usersCall.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Response<UsersResponse> response, Retrofit retrofit) {
                hideActivityProgress();
                if(response != null && response.isSuccess()) {
                    users = response.body().getUsers();
                    friendsAdapter.swapContent(users);
                } else {
                    showGeneralErroDialog();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                hideActivityProgress();
                showGeneralErroDialog();
            }
        });
    }
}
