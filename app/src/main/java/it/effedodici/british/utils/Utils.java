package it.effedodici.british.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import it.effedodici.british.models.Challange;
import it.effedodici.british.models.FBUser;
import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 09/11/16.
 */
public class Utils {

    public static User getUserFromDefaults(Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        User.UserType type = User.UserType.valueOf(preferences.getString(Constants.LOGIN_TYPE, "UNDEFINED"));

        if(type == User.UserType.FACEBOOK) {
            FBUser user = new FBUser();
            user.setId(preferences.getString(Constants.ID_PREF_KEY, ""));
            user.setEmail(preferences.getString(Constants.EMAIL_PREF_KEY, ""));
            user.setUsername(preferences.getString(Constants.USERNAME_PREF_KEY, ""));
            user.setCreated(preferences.getString(Constants.CREATED_PREF_KEY, ""));
            user.setPicture_url(preferences.getString(Constants.PICTURE_URL_PREF_KEY, ""));
            user.setPoints(preferences.getInt(Constants.POINTS_PREF_KEY, 0));
            user.setType(type);
            user.setFbToken(preferences.getString(Constants.FB_TOKEN, ""));
            user.setFCMToken(preferences.getString(Constants.FCM_TOKEN_PREF_KEY, ""));
            return user;
        } else {
            User user = new User();
            user.setId(preferences.getString(Constants.ID_PREF_KEY, ""));
            user.setEmail(preferences.getString(Constants.EMAIL_PREF_KEY, ""));
            user.setUsername(preferences.getString(Constants.USERNAME_PREF_KEY, ""));
            user.setCreated(preferences.getString(Constants.CREATED_PREF_KEY, ""));
            user.setPicture_url(preferences.getString(Constants.PICTURE_URL_PREF_KEY, ""));
            user.setPoints(preferences.getInt(Constants.POINTS_PREF_KEY, 0));
            user.setType(type);
            user.setFCMToken(preferences.getString(Constants.FCM_TOKEN_PREF_KEY, ""));
            return user;
        }
    }

    public static void saveUserToPreferences(Context ctx, User user) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor edit = preferences.edit();
                edit.putString(Constants.ID_PREF_KEY, user.getId())
                .putString(Constants.EMAIL_PREF_KEY, user.getEmail())
                .putString(Constants.USERNAME_PREF_KEY, user.getUsername())
                .putString(Constants.LOGIN_TYPE, user.getType().toString())
                .putString(Constants.CREATED_PREF_KEY, user.getCreated())
                .putString(Constants.PICTURE_URL_PREF_KEY, user.getPicture_url())
                .putInt(Constants.POINTS_PREF_KEY, user.getPoints())
                .putString(Constants.FCM_TOKEN_PREF_KEY, user.getFCMToken());
        if(user.getType() == User.UserType.FACEBOOK && user instanceof FBUser) {
            edit.putString(Constants.FB_TOKEN, ((FBUser) user).getFbToken());
        }
        edit.apply();
    }

    public static void deleteUser(Context ctx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor edit = preferences.edit();
        edit.remove(Constants.ID_PREF_KEY)
                .remove(Constants.EMAIL_PREF_KEY)
                .remove(Constants.USERNAME_PREF_KEY)
                .remove(Constants.LOGIN_TYPE)
                .remove(Constants.CREATED_PREF_KEY)
                .remove(Constants.PICTURE_URL_PREF_KEY)
                .remove(Constants.FB_TOKEN)
                .remove(Constants.POINTS_PREF_KEY)
                .remove(Constants.FCM_TOKEN_PREF_KEY)
                .apply();
    }
}
