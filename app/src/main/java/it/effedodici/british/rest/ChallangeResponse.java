package it.effedodici.british.rest;

import it.effedodici.british.models.Challange;

/**
 * Created by mirkopinna on 07/02/17.
 */

public class ChallangeResponse {

    private Challange challange;

    public Challange getChallange() {
        return challange;
    }

    public void setChallange(Challange challange) {
        this.challange = challange;
    }
}
