package it.effedodici.british.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.okhttp.internal.Util;

import it.effedodici.british.BaseActivity;
import it.effedodici.british.R;
import it.effedodici.british.models.Challange;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.rest.StartChallangeResponse;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 18/11/16.
 */
public class FinalWinFragment extends GameFragment {

    private Challange challange;

    private TextView yourLbl;
    private TextView topLbl;
    private ImageView finalImg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.final_game_fragment, container, false);

        topLbl = (TextView) root.findViewById(R.id.final_top_lbl);
        yourLbl = (TextView) root.findViewById(R.id.final_game_you_earn_lbl);
        finalImg = (ImageView) root.findViewById(R.id.final_image);

        if(challange != null) {

            boolean amIStarter = challange.getStarter().getId().equals(Utils.getUserFromDefaults(getActivity()).getId());
            if(amIStarter) {
                if(challange.getStarterState().getAnswers() >= 5) {
                    //You won
                    setUpWinnerScreen(challange.getStarterState().getPoints());
                } else {
                    //You lose
                    setUpLoserScreen(challange.getStarterState().getPoints());
                }
            } else {
                if(challange.getStarterState().getAnswers() >= 5) {
                    setUpLoserScreen(challange.getChallanger().getPoints());
                } else {
                    setUpWinnerScreen(challange.getChallanger().getPoints());
                }
            }
        }

        root.findViewById(R.id.final_game_leave_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() != null && getActivity() instanceof BaseActivity) {
                    requestClearBackStack(HomeFragment.TAG);
                }
            }
        });

        return root;
    }

    private void setUpLoserScreen(int points) {
        topLbl.setText(getString(R.string.you_lose));
        yourLbl.setText(getString(R.string.you_got_only, points) + "\n" + getString(R.string.study_more));
        finalImg.setImageResource(R.drawable.final_lose);
    }

    private void setUpWinnerScreen(int points) {
        topLbl.setText(getString(R.string.congrats));
        yourLbl.setText(getString(R.string.you_earn, points));
        finalImg.setImageResource(R.drawable.final_win);
    }

    public Challange getChallange() {
        return challange;
    }

    public void setChallange(Challange challange) {
        this.challange = challange;
    }

    @Override
    public String getFragmentTag() {
        return "FinalWin";
    }
}
