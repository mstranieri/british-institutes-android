package it.effedodici.british.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import it.effedodici.british.R;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.PictureResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.SelectedFilePath;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by mirkopinna on 30/08/16.
 */
public class ProfileFragment extends ToolBarFragment {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private User user;

    private TextView profileName;
    private TextView totalPoints;
    private TextView bestMatch;
    private TextView bestQuestion;
    private TextView totalWins, totalLost, totalDraw, totalMatch;
    private ImageButton profileBtn;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {

            showActivityProgress();
            final Uri imageUri = data.getData();

            File image = new File(SelectedFilePath.getPath(getActivity(), imageUri));
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", "picture.jpg", RequestBody.create(MediaType.parse("image/*"), image));
            RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), user.getId());

            retrofit2.Call<PictureResponse> pictureCall = RetrofitManager.getApiService2(Constants.BASE_URL).uploadPicture(filePart, user_id);
            pictureCall.enqueue(new retrofit2.Callback<PictureResponse>() {
                @Override
                public void onResponse(retrofit2.Call<PictureResponse> call, retrofit2.Response<PictureResponse> response) {
                    if (response != null && response.isSuccessful()) {
                        Picasso.with(getActivity()).load(imageUri).into(profileBtn);
                        user.setPicture_url(response.body().getPicture_url());
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        preferences.edit().putString(Constants.PICTURE_URL_PREF_KEY, response.body().getPicture_url()).apply();
                    }
                    hideActivityProgress();
                }

                @Override
                public void onFailure(retrofit2.Call<PictureResponse> call, Throwable t) {
                    showGeneralErroDialog();
                    hideActivityProgress();
                }
            });
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.profile_fragment, container, false);

        profileName = (TextView) root.findViewById(R.id.profile_name);
        profileBtn = (ImageButton) root.findViewById(R.id.profile_picture);
        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromGallery();
            }
        });

        totalPoints = (TextView) root.findViewById(R.id.profile_statistics_total_points);
        bestMatch = (TextView) root.findViewById(R.id.profile_statistics_best_match);
        bestQuestion = (TextView) root.findViewById(R.id.profile_statistics_best_question);
        totalWins = (TextView) root.findViewById(R.id.profile_statistics_wins);
        totalLost = (TextView) root.findViewById(R.id.profile_statistics_lost);
        totalDraw = (TextView) root.findViewById(R.id.profile_statistics_draw);
        totalMatch = (TextView) root.findViewById(R.id.profile_statistics_total_match);


        setUpToolBar(root, getString(R.string.profile_title));

        setUpUser();

        return root;

    }

    public void setUser(User user) {
        this.user = user;
    }

    private void setUpUser() {
        if(user != null) {
            profileName.setText(user.getUsername());
            if(!TextUtils.isEmpty(user.getPicture_url())) {
                Picasso.with(getActivity()).load(user.getPicture_url()).into(profileBtn);
            }
            totalPoints.setText(String.valueOf(user.getPoints()));
        }
    }

    private void getImageFromGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
    }
}
