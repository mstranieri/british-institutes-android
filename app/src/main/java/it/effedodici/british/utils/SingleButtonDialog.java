package it.effedodici.british.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by mirko on 07/04/15.
 */
public class SingleButtonDialog extends android.support.v4.app.DialogFragment {

    public interface SingleButtonListener {
        void onPositiveButtonClick();
    }

    private String mTitle;
    private String mMessage;

    private SingleButtonListener mListener = null;

    public SingleButtonDialog(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle(mTitle);
        dialogBuilder.setMessage(mMessage);
        dialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(mListener != null) {
                    mListener.onPositiveButtonClick();
                }
            }
        });


        return dialogBuilder.create();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public void setListener(SingleButtonListener listener) {
        this.mListener = listener;
    }
}
