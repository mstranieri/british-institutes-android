package it.effedodici.british;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.Profile;

import it.effedodici.british.fragments.BaseFragment;
import it.effedodici.british.fragments.LoginFragment;
import it.effedodici.british.models.User;
import it.effedodici.british.utils.Utils;

/**
 * Created by mirkopinna on 25/08/16.
 */
public class LoginActivity extends BaseActivity {

    private ProgressBar loginProgress;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        loginProgress = (ProgressBar) findViewById(R.id.activity_login_progress);

        replaceFragment(new LoginFragment(), null, false);

        /*User user = Utils.getUserFromDefaults(this);
        if(!TextUtils.isEmpty(user.getId())) {
            //User already did login go to home.
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            //User needs to login
            //Show login fragment
            replaceFragment(new LoginFragment(), false);
        }*/
    }

    @Override
    public int getFragmentContainerId() {
        return R.id.login_main_frame;
    }

    @Override
    public ProgressBar getProgressBar() {
        return null;
    }

    @Override
    public String getBaseFragmentTag() {
        return null;
    }
}
