package it.effedodici.british.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.effedodici.british.R;
import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 22/11/16.
 */
public class FriendsAdapter extends BaseAdapter {

    private List<User> users = new ArrayList<>();

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.friends_grid_row, parent, false);
        User opponent = users.get(position);
        if(opponent != null) {
            ((TextView)root.findViewById(R.id.friends_grid_username)).setText(opponent.getUsername());
            ImageView picture = (ImageView)root.findViewById(R.id.friends_grid_image);
            if(!TextUtils.isEmpty(opponent.getPicture_url())) {
                Picasso.with(parent.getContext()).load(opponent.getPicture_url()).into(picture);
            }
        }
        return root;
    }

    public void swapContent(List<User> users) {
        this.users.clear();
        if(users != null) {
            this.users.addAll(users);
        }
        notifyDataSetChanged();
    }

}
