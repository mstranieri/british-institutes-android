package it.effedodici.british;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import it.effedodici.british.fragments.CategoriesFragment;
import it.effedodici.british.fragments.HomeFragment;
import it.effedodici.british.fragments.UserChallangesFragment;
import it.effedodici.british.models.Challange;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.ChallangeResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends BaseActivity {

    ProgressBar mainProgress;

    private Challange commonChallenge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainProgress = (ProgressBar) findViewById(R.id.main_activity_progress);

        replaceFragment(new HomeFragment(), null, false);

        Intent startingIntent = getIntent();
        if(startingIntent != null) {
            String challangeId = startingIntent.getStringExtra("challange_id");
            Call<ChallangeResponse> challangeCall = RetrofitManager.getApiService(Constants.BASE_URL).getChallange(challangeId);
            challangeCall.enqueue(new Callback<ChallangeResponse>() {
                @Override
                public void onResponse(Response<ChallangeResponse> response, Retrofit retrofit) {
                    if(response.isSuccess()) {
                        CategoriesFragment fragment = new CategoriesFragment();
                        fragment.setChallange(response.body().getChallange());
                        replaceFragment(fragment, HomeFragment.TAG, true);
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

        }

    }

    public Challange getCommonChallenge() {
        return commonChallenge;
    }

    public void setCommonChallenge(Challange commonChallenge) {
        this.commonChallenge = commonChallenge;
    }

    @Override
    public int getFragmentContainerId() {
        return R.id.main_frame_container;
    }

    @Override
    public ProgressBar getProgressBar() {
        return mainProgress;
    }

    @Override
    public String getBaseFragmentTag() {
        return HomeFragment.class.getSimpleName();
    }

}
