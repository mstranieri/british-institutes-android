package it.effedodici.british.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mirkopinna on 18/11/16.
 */
public class GameState {

    private int answers;
    private int points;
    @SerializedName("selected_categories")
    private List<String> selectedCategories;

    public int getAnswers() {
        return answers;
    }

    public void setAnswers(int answers) {
        this.answers = answers;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<String> getSelectedCategories() {
        return selectedCategories;
    }

    public void setSelectedCategories(List<String> selectedCategories) {
        this.selectedCategories = selectedCategories;
    }
}
