package it.effedodici.british.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import it.effedodici.british.R;
import it.effedodici.british.models.Challange;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.rest.StartChallangeResponse;
import it.effedodici.british.rest.UserChallangesResponse;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.SingleButtonDialog;
import it.effedodici.british.utils.Utils;
import it.effedodici.british.utils.YesNoButtonDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 10/11/16.
 */
public class OpponentDescriptionFragment extends ToolBarFragment {

    private User opponent;

    private boolean hasOpenChallenge = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.opponent_description, container, false);

        setUpToolBar(root, getString(R.string.opponent_title));

        final String myID = Utils.getUserFromDefaults(getActivity()).getId();

        if(opponent != null) {
            ((TextView)root.findViewById(R.id.opponent_description_name)).setText(opponent.getUsername());
            ImageView picture = (ImageView) root.findViewById(R.id.opponent_description_picture);
            if(!TextUtils.isEmpty(opponent.getPicture_url())) {
                Picasso.with(getActivity()).load(opponent.getPicture_url()).into(picture);
            }
            ((TextView)root.findViewById(R.id.opponent_description_total_points)).setText(String.valueOf(opponent.getPoints()));
        }

        root.findViewById(R.id.opponent_description_play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!hasOpenChallenge) {
                    YesNoButtonDialog dialog = new YesNoButtonDialog();
                    dialog.setMessage(getString(R.string.send_game_request, opponent.getUsername()));
                    dialog.setListener(new YesNoButtonDialog.YesNoButtonListener() {
                        @Override
                        public void onPositiveButtonClick() {
                            //Show categories
                            Call<StartChallangeResponse> startCall = RetrofitManager.getApiService(Constants.BASE_URL).startChallange(myID, opponent.getId());
                            startCall.enqueue(new Callback<StartChallangeResponse>() {
                                @Override
                                public void onResponse(Response<StartChallangeResponse> response, Retrofit retrofit) {
                                    if (response != null && response.isSuccess()) {
                                        Challange challange = response.body().getChallange();
                                        CategoriesFragment catFragment = new CategoriesFragment();
                                        catFragment.setChallange(challange);
                                        requestFragmentReplacement(catFragment, null, true);
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    showGeneralErroDialog();
                                }
                            });
                        }
                        @Override
                        public void onNegativeButtonClick() {
                            //NOPE
                        }
                    });
                    dialog.show(getChildFragmentManager(), "do_you_challange");
                } else {
                    SingleButtonDialog dialog = new SingleButtonDialog();
                    dialog.setMessage("You already started a challenge with this user. You can continue it from the challanges list.");
                    dialog.setListener(new SingleButtonDialog.SingleButtonListener() {
                        @Override
                        public void onPositiveButtonClick() {
                            //NOPE
                        }
                    });
                    dialog.show(getChildFragmentManager(), "challange_started_dialog");
                }
            }
        });

        showActivityProgress();
        Call<UserChallangesResponse> challengesCall = RetrofitManager.getApiService(Constants.BASE_URL).userChallanges(myID);
        challengesCall.enqueue(new Callback<UserChallangesResponse>() {
            @Override
            public void onResponse(Response<UserChallangesResponse> response, Retrofit retrofit) {
                hideActivityProgress();
                if(response.isSuccess()) {
                    for(Challange challenge : response.body().getChallanges()) {
                        if(challenge.getStarter().getId().equals(opponent.getId()) || challenge.getChallanger().getId().equals(opponent.getId())) {
                            hasOpenChallenge = true;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                hideActivityProgress();
                showGeneralErroDialog();
            }
        });

        return root;
    }

    public User getOpponent() {
        return opponent;
    }

    public void setOpponent(User opponent) {
        this.opponent = opponent;
    }
}
