package it.effedodici.british.rest;

import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 09/11/16.
 */
public class LoginResponse {

    private boolean logged;
    private User user;

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
