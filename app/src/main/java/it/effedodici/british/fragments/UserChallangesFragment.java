package it.effedodici.british.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.effedodici.british.R;
import it.effedodici.british.adapter.ChallangesAdapter;
import it.effedodici.british.models.Challange;
import it.effedodici.british.rest.ChallangeResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.rest.UserChallangesResponse;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 16/11/16.
 */
public class UserChallangesFragment extends ToolBarFragment {

    private RecyclerView mRecyclerView;
    private ChallangesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private BroadcastReceiver opponentActionReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.user_challanges_fragment, container, false);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.challanges_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ChallangesAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setListener(new ChallangesAdapter.ChallangesAdapterListener() {
            @Override
            public void onItemClicked(Challange challnge) {
                //Start Category fragment
                CategoriesFragment fragm = new CategoriesFragment();
                fragm.setChallange(challnge);
                requestFragmentReplacement(fragm, null, true);
            }
        });

        setUpToolBar(root, getString(R.string.challanges_title));
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        opponentActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //Decrease notifications number as we are into the game already
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                int notifications = preferences.getInt(Constants.CHALLENGE_NOTIFICATIONS, 0);
                if(notifications > 0) {
                    preferences.edit().putInt(Constants.CHALLENGE_NOTIFICATIONS, notifications-1).apply();
                }

                refreshData();
            }
        };

        IntentFilter challangeFilter = new IntentFilter(Constants.OPPONENT_RESPONDED);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(opponentActionReceiver, challangeFilter);

        //Decrease notifications number as we are into the game already
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int notifications = preferences.getInt(Constants.CHALLENGE_NOTIFICATIONS, 0);
        if(notifications > 0) {
            preferences.edit().putInt(Constants.CHALLENGE_NOTIFICATIONS, 0).apply();
        }

        refreshData();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(opponentActionReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(opponentActionReceiver);
            opponentActionReceiver = null;
        }
    }

    private void refreshData() {
        showActivityProgress();
        Call<UserChallangesResponse> challangesCall = RetrofitManager.getApiService(Constants.BASE_URL).userChallanges(Utils.getUserFromDefaults(getActivity()).getId());
        challangesCall.enqueue(new Callback<UserChallangesResponse>() {
            @Override
            public void onResponse(Response<UserChallangesResponse> response, Retrofit retrofit) {
                hideActivityProgress();
                if(response != null && response.isSuccess()) {
                    mAdapter.swapData(response.body().getChallanges());
                } else {
                    showGeneralErroDialog();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                hideActivityProgress();
                showGeneralErroDialog();
            }
        });
    }
}
