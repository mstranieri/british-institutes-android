package it.effedodici.british.rest;

import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 09/11/16.
 */
public class RegistrationResponse {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
