package it.effedodici.british.rest;

import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 10/11/16.
 */
public class RandomOpponentResponse {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
