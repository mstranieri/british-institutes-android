package it.effedodici.british.rest;

import java.util.List;

import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 22/11/16.
 */
public class UsersResponse {
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
