package it.effedodici.british.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class YesNoButtonDialog extends DialogFragment {
	
	public interface YesNoButtonListener {
		void onPositiveButtonClick();
		void onNegativeButtonClick();
	}
	
	private String mTitle;
    private String mMessage;
    
    private YesNoButtonListener mListener;
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle(mTitle);
        dialogBuilder.setMessage(mMessage);
        dialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(mListener != null) {
                    mListener.onPositiveButtonClick();
                }
            }
        });
        
        dialogBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(mListener != null) {
					mListener.onNegativeButtonClick();
				}
			}
		});

        return dialogBuilder.create();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public void setListener(YesNoButtonListener listener) {
        this.mListener = listener;
    }

}
