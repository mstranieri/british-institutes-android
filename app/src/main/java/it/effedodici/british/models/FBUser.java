package it.effedodici.british.models;

import com.facebook.Profile;

/**
 * Created by mirkopinna on 02/12/16.
 */
public class FBUser extends User{

    private String fbToken;

    @Override
    public String getPicture_url() {
        /*Profile profile = Profile.getCurrentProfile();
        if(profile != null) {
            return profile.getProfilePictureUri(200, 200).toString();
        }*/
        return super.getPicture_url();
    }

    public String getFbToken() {
        return fbToken;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }
}
