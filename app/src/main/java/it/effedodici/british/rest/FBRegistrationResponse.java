package it.effedodici.british.rest;

import it.effedodici.british.models.FBUser;
import it.effedodici.british.models.User;

/**
 * Created by mirkopinna on 02/12/16.
 */
public class FBRegistrationResponse {
    private FBUser user;

    public FBUser getUser() {
        return user;
    }

    public void setUser(FBUser user) {
        this.user = user;
    }
}
