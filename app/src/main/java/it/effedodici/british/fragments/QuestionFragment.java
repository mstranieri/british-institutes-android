package it.effedodici.british.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.okhttp.Challenge;

import java.util.ArrayList;

import it.effedodici.british.BaseActivity;
import it.effedodici.british.MainActivity;
import it.effedodici.british.R;
import it.effedodici.british.models.Challange;
import it.effedodici.british.models.Question;
import it.effedodici.british.rest.AnswerResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import it.effedodici.british.utils.YesNoButtonDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 15/11/16.
 */
public class QuestionFragment extends GameFragment {

    private Challange challange;
    private Question question;
    private Button first, second, third, fourth;

    private CountDownTimer timer;
    private ProgressBar questionProgress;
    private TextView questionTime;

    private boolean canGoToNext = false;

    Challange updatedChallange = null;
    boolean hasUpdatedChallange = false;
    boolean isCorrect = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.question_fragment, container, false);

        root.findViewById(R.id.custom_action_bar_back_btn).setVisibility(View.GONE);
        boolean isStarter = challange.getStarter().getId().equals(Utils.getUserFromDefaults(getActivity()).getId());
        ((TextView)root.findViewById(R.id.custom_action_bar_title)).setText(getString(R.string.question,
                isStarter ? challange.getStarterState().getSelectedCategories().size() : challange.getChallangerState().getSelectedCategories().size()));

        if(question != null) {

            questionProgress = (ProgressBar) root.findViewById(R.id.question_time_progress);
            questionProgress.setMax(30);

            questionTime = (TextView) root.findViewById(R.id.question_time_lbl);

            ((TextView)root.findViewById(R.id.question_question_lbl)).setText(question.getQuestion());

            ArrayList<String> answers = question.getAnswers();
            if(answers != null && answers.size() > 0) {
                first = (Button)root.findViewById(R.id.question_first_answer);
                first.setText(answers.get(0));
                first.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //First answer
                        checkAnswer(1, v);
                    }
                });

                second = (Button)root.findViewById(R.id.question_second_answer);
                second.setText(answers.get(1));
                second.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Second answer
                        checkAnswer(2, v);
                    }
                });

                third = (Button)root.findViewById(R.id.question_third_answer);
                third.setText(answers.get(2));
                third.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Third
                        checkAnswer(3, v);
                    }
                });

                fourth = (Button)root.findViewById(R.id.question_fourth_answer);
                fourth.setText(answers.get(3));
                fourth.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Fourth
                        checkAnswer(4, v);
                    }
                });
            }
        }
        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onStart() {
        super.onStart();


        timer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("Tick..", String.valueOf((int) millisUntilFinished / 1000));
                int time = (int) millisUntilFinished / 1000;
                questionProgress.setProgress(time);
                questionTime.setText(String.valueOf(time) + "\"");
            }

            @Override
            public void onFinish() {
                questionProgress.setProgress(0);
                questionTime.setText("0\"");

                switch (question.getCorrect()) {
                    case 1:
                        first.setBackgroundResource(R.drawable.right_answer_button);
                        break;
                    case 2:
                        second.setBackgroundResource(R.drawable.right_answer_button);
                        break;
                    case 3:
                        third.setBackgroundResource(R.drawable.right_answer_button);
                        break;
                    case 4:
                        fourth.setBackgroundResource(R.drawable.right_answer_button);
                        break;
                }

                checkAnswer(300, null);
            }
        };
        timer.start();

    }

    private void checkAnswer(int selected, View selectedView) {
        if(timer != null) {
            timer.cancel();
        }

        isCorrect = (selected == question.getCorrect());
        if(isCorrect) {
            selectedView.setBackgroundResource(R.drawable.right_answer_button);
        }else {

            if(selectedView != null) {
                selectedView.setBackgroundResource(R.drawable.wrong_answer_button);
            }

            switch (question.getCorrect()) {
                case 1:
                    first.setBackgroundResource(R.drawable.right_answer_button);
                    break;
                case 2:
                    second.setBackgroundResource(R.drawable.right_answer_button);
                    break;
                case 3:
                    third.setBackgroundResource(R.drawable.right_answer_button);
                    break;
                case 4:
                    fourth.setBackgroundResource(R.drawable.right_answer_button);
                    break;
            }
        }

        showActivityProgress();
        new CountDownTimer(3000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                //Go to loose fragment
                canGoToNext = true;
                if(hasUpdatedChallange && updatedChallange != null) {

                    goToFinalScreen(updatedChallange);

                }
            }
        }.start();

        Call<AnswerResponse> callAnswer = RetrofitManager.getApiService(Constants.BASE_URL).answer(isCorrect ? "yes" : "no", challange.getId(), Utils.getUserFromDefaults(getActivity()).getId(), question.getPoints());
        callAnswer.enqueue(new Callback<AnswerResponse>() {
            @Override
            public void onResponse(Response<AnswerResponse> response, Retrofit retrofit) {
                hideActivityProgress();
                if (response != null && response.isSuccess()) {
                    hasUpdatedChallange = true;
                    updatedChallange = response.body().getChallange();

                    //Set the challenge in common between fragments
                    if(getActivity() instanceof MainActivity) {
                        ((MainActivity)getActivity()).setCommonChallenge(updatedChallange);
                    }

                    goToFinalScreen(response.body().getChallange());

                } else {
                    showGeneralErroDialog();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                hideActivityProgress();
                showGeneralErroDialog();
            }
        });
    }

    private void goToFinalScreen(Challange challange) {
        if (challange.isTerminated()) {
            //Show final fragment
            FinalWinFragment finalFrag = new FinalWinFragment();
            finalFrag.setChallange(challange);
            requestFragmentReplacement(finalFrag, null, true);
        } else {

            if (challange.getStarter().getId().equals(Utils.getUserFromDefaults(getActivity()).getId())) {
                Utils.saveUserToPreferences(getActivity(), challange.getStarter());
            } else if (challange.getChallanger().getId().equals(Utils.getUserFromDefaults(getActivity()).getId())) {
                Utils.saveUserToPreferences(getActivity(), challange.getChallanger());
            }

            if (canGoToNext) {
                if(isCorrect) {
                    WinFragment fragm = new WinFragment();
                    fragm.setQuestion(question);
                    requestFragmentReplacement(fragm, null, true);
                } else {
                    LoseFragment fragm = new LoseFragment();
                    fragm.setQuestion(question);
                    requestFragmentReplacement(fragm, null, true);
                }

            }
        }
    }

    @Override
    public void actionOnBackPress() {
        YesNoButtonDialog dialog = new YesNoButtonDialog();
        dialog.setMessage(getString(R.string.game_back_press_message));
        dialog.setListener(new YesNoButtonDialog.YesNoButtonListener() {
            @Override
            public void onPositiveButtonClick() {
                if(timer != null) {
                    timer.cancel();
                    Call<AnswerResponse> answerCall = RetrofitManager.getApiService(Constants.BASE_URL).answer("no", challange.getId(),
                            Utils.getUserFromDefaults(getActivity()).getId(), question.getPoints());
                    answerCall.enqueue(new Callback<AnswerResponse>() {
                        @Override
                        public void onResponse(Response<AnswerResponse> response, Retrofit retrofit) {
                            if (getActivity() instanceof BaseActivity) {
                                ((BaseActivity) getActivity()).popBackStack();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            showGeneralErroDialog();
                        }
                    });
                }
            }

            @Override
            public void onNegativeButtonClick() {

            }
        });
        dialog.show(getChildFragmentManager(), "leave_message");
    }

    public void setChallange(Challange challange) {
        this.challange = challange;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
