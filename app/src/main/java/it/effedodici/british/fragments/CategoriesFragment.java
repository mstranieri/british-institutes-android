package it.effedodici.british.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.okhttp.Challenge;

import java.util.List;

import it.effedodici.british.MainActivity;
import it.effedodici.british.R;
import it.effedodici.british.models.Challange;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.ChallangeResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.rest.SelectCategoryResponse;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.Utils;
import it.effedodici.british.utils.YesNoButtonDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 10/11/16.
 */
public class CategoriesFragment extends GameFragment {

    public static final String TAG = "Categories";

    private Challange challange;

    ImageButton culture, food, geography, gossip, music, science, story, tv, sport;

    ProgressBar opponentSpinner;

    private TextView categoryLbl;

    private BroadcastReceiver opponentActionReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.categories_fragment, container, false);

        culture = (ImageButton) root.findViewById(R.id.category_culture);
        food =  (ImageButton) root.findViewById(R.id.category_food);
        geography = (ImageButton)root.findViewById(R.id.category_geography);
        gossip = (ImageButton)root.findViewById(R.id.category_gossip);
        music = (ImageButton)root.findViewById(R.id.category_music);
        science = (ImageButton)root.findViewById(R.id.category_science);
        story = (ImageButton)root.findViewById(R.id.category_story);
        tv = (ImageButton)root.findViewById(R.id.category_tv);
        sport = (ImageButton)root.findViewById(R.id.category_sport);

        categoryLbl = (TextView) root.findViewById(R.id.category_lbl);
        opponentSpinner = (ProgressBar) root.findViewById(R.id.categories_opponent_spinner);

        return root;
    }


    @Override
    public void onPause() {
        super.onPause();
        if(opponentActionReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(opponentActionReceiver);
            opponentActionReceiver = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        opponentActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                showActivityProgress();

                if(intent.getStringExtra(Constants.CHALLANGE_ID).equals(challange.getId())) {
                    //Decrease notifications number as we are into the game already
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    int notifications = preferences.getInt(Constants.CHALLENGE_NOTIFICATIONS, 0);
                    if(notifications > 0) {
                        preferences.edit().putInt(Constants.CHALLENGE_NOTIFICATIONS, notifications-1).apply();
                    }

                    Call<ChallangeResponse> challangeCall = RetrofitManager.getApiService(Constants.BASE_URL).getChallange(challange.getId());
                    challangeCall.enqueue(new Callback<ChallangeResponse>() {
                        @Override
                        public void onResponse(Response<ChallangeResponse> response, Retrofit retrofit) {
                            hideActivityProgress();
                            if(response.isSuccess()) {
                                challange = response.body().getChallange();
                                if(challange.isTerminated()) {
                                    //Go to final Screen
                                    FinalWinFragment finalFrag = new FinalWinFragment();
                                    finalFrag.setChallange(challange);
                                    requestFragmentReplacement(finalFrag, null, false);
                                } else {
                                    setUpChallange();
                                    setSelectedCategories();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            hideActivityProgress();
                            showGeneralErroDialog();
                        }
                    });
                }
            }
        };

        IntentFilter challangeFilter = new IntentFilter(Constants.OPPONENT_RESPONDED);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(opponentActionReceiver, challangeFilter);

        if(getActivity() instanceof MainActivity) {
            Challange commonCh = ((MainActivity)getActivity()).getCommonChallenge();
            if(commonCh != null) {
                this.challange = commonCh;
            }
        }

        setUpChallange();
        setSelectedCategories();

    }

    @Override
    public void actionOnBackPress() {

        YesNoButtonDialog ynDialog = new YesNoButtonDialog();
        ynDialog.setMessage(getString(R.string.cat_go_back_home));
        ynDialog.setListener(new YesNoButtonDialog.YesNoButtonListener() {
            @Override
            public void onPositiveButtonClick() {
                requestGoHome();
            }

            @Override
            public void onNegativeButtonClick() {

            }
        });
        ynDialog.show(getChildFragmentManager(), "back_home");

    }

    //culture, food, geography, gossip, music, science, story, tv;
    private void setSelectedCategories() {
        if(challange != null) {
            List<String> categories = null;

            if(challange.getStarter().getId().equals(Utils.getUserFromDefaults(getActivity()).getId())) {
                categories = challange.getStarterState().getSelectedCategories();
            } else {
                categories = challange.getChallangerState().getSelectedCategories();
            }

            if(categories != null) {
                for(String category : categories) {
                    if(category.equals(Constants.CULTURE)) {
                        culture.setOnClickListener(null);
                        culture.setImageResource(R.drawable.culture_disabled);
                    } else if(category.equals(Constants.FOOD)) {
                        food.setOnClickListener(null);
                        food.setImageResource(R.drawable.food_disabled);
                    } else if(category.equals(Constants.GEOGRAPHY)) {
                        geography.setOnClickListener(null);
                        geography.setImageResource(R.drawable.geography_disabled);
                    } else if(category.equals(Constants.GOSSIP)) {
                        gossip.setOnClickListener(null);
                        gossip.setImageResource(R.drawable.gossip_disabled);
                    } else if(category.equals(Constants.MUSIC)) {
                        music.setOnClickListener(null);
                        music.setImageResource(R.drawable.music_disabled);
                    } else if(category.equals(Constants.SCIENCE)) {
                        science.setOnClickListener(null);
                        science.setImageResource(R.drawable.science_disabled);
                    } else if(category.equals(Constants.HISTORY)) {
                        story.setOnClickListener(null);
                        story.setImageResource(R.drawable.story_disabled);
                    } else if(category.equals(Constants.TV_CINEMA)) {
                        tv.setOnClickListener(null);
                        tv.setImageResource(R.drawable.tv_disabled);
                    }

                }
            }
        }
    }

    private void gotoNextSection(String category) {
        if(challange != null) {
            Call<SelectCategoryResponse> catCall = RetrofitManager.getApiService(Constants.BASE_URL).selectCategory(Utils.getUserFromDefaults(getActivity()).getId(), challange.getId(), category);
            catCall.enqueue(new Callback<SelectCategoryResponse>() {
                @Override
                public void onResponse(Response<SelectCategoryResponse> response, Retrofit retrofit) {
                    if(response != null && response.isSuccess()) {
                        //Go to question fragment
                        QuestionFragment fragm = new QuestionFragment();
                        fragm.setChallange(response.body().getChallange());
                        fragm.setQuestion(response.body().getQuestion());
                        requestFragmentReplacement(fragm, CategoriesFragment.TAG, true);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    showGeneralErroDialog();
                }
            });
        }
    }

    private void setUpChallange() {

        User me = Utils.getUserFromDefaults(getActivity());
        if(me.getId().equals(challange.getNextMove())) {

            categoryLbl.setText(R.string.your_tourn);
            opponentSpinner.setVisibility(View.GONE);

            culture.setImageResource(R.drawable.culture);
            culture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Culture selected
                    gotoNextSection(Constants.CULTURE);
                }
            });

            food.setImageResource(R.drawable.food);
            food.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Food selected
                    gotoNextSection(Constants.FOOD);
                }
            });

            geography.setImageResource(R.drawable.geography);
            geography.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Geography selected
                    gotoNextSection(Constants.GEOGRAPHY);
                }
            });

            gossip.setImageResource(R.drawable.gossip);
            gossip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Gossip
                    gotoNextSection(Constants.GOSSIP);
                }
            });

            music.setImageResource(R.drawable.music);
            music.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Music
                    gotoNextSection(Constants.MUSIC);
                }
            });

            science.setImageResource(R.drawable.science);
            science.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //SCience
                    gotoNextSection(Constants.SCIENCE);
                }
            });

            story.setImageResource(R.drawable.story);
            story.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Story
                    gotoNextSection(Constants.HISTORY);
                }
            });

            tv.setImageResource(R.drawable.tv);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TV
                    gotoNextSection(Constants.TV_CINEMA);
                }
            });

            sport.setImageResource(R.drawable.sport);
            sport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Sport
                    gotoNextSection(Constants.SPORT);
                }
            });
        } else {
            categoryLbl.setText(R.string.opponent_turn);
            opponentSpinner.setVisibility(View.VISIBLE);
            culture.setImageResource(R.drawable.culture_obs);
            food.setImageResource(R.drawable.food_obs);
            geography.setImageResource(R.drawable.geography_obs);
            gossip.setImageResource(R.drawable.gossip_obs);
            music.setImageResource(R.drawable.music_obs);
            science.setImageResource(R.drawable.science_obs);
            story.setImageResource(R.drawable.story_obs);
            tv.setImageResource(R.drawable.tv_obs);
            sport.setImageResource(R.drawable.sport_obs);
        }
    }

    public Challange getChallange() {
        return challange;
    }

    public void setChallange(Challange challange) {
        this.challange = challange;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
