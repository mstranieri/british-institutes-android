package it.effedodici.british.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.facebook.Profile;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;

import it.effedodici.british.MainActivity;
import it.effedodici.british.R;
import it.effedodici.british.models.FBUser;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.FBRegistrationResponse;
import it.effedodici.british.rest.PictureResponse;
import it.effedodici.british.rest.RegistrationResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.SelectedFilePath;
import it.effedodici.british.utils.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 09/11/16.
 */
public class RegistrationFragment extends BaseFragment {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private ImageButton pictBtn;
    private Button registerBtn;
    private EditText usernameET;

    private User user;

    private Profile fbProfile;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {

            showActivityProgress();
            final Uri imageUri = data.getData();

            File image = new File(SelectedFilePath.getPath(getActivity(), imageUri));
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", "picture.jpg", RequestBody.create(MediaType.parse("image/*"), image));
            RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), "profile_" + String.valueOf((int)Math.floor(Math.random())));

            retrofit2.Call<PictureResponse> pictureCall = RetrofitManager.getApiService2(Constants.BASE_URL).uploadPicture(filePart, user_id);
            pictureCall.enqueue(new retrofit2.Callback<PictureResponse>() {
                @Override
                public void onResponse(retrofit2.Call<PictureResponse> call, retrofit2.Response<PictureResponse> response) {
                    if (response != null && response.isSuccessful()) {
                        Picasso.with(getActivity()).load(imageUri).into(pictBtn);
                        user.setPicture_url(response.body().getPicture_url());
                    }

                    registerBtn.setEnabled(true);
                    hideActivityProgress();
                }

                @Override
                public void onFailure(retrofit2.Call<PictureResponse> call, Throwable t) {
                    showGeneralErroDialog();

                    registerBtn.setEnabled(true);
                    hideActivityProgress();
                }
            });
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.registration_fragment, container, false);

        usernameET = (EditText) root.findViewById(R.id.registration_username_field);

        pictBtn = (ImageButton) root.findViewById(R.id.registration_picture_btn);
        pictBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dispatchTakePictureIntent();
                registerBtn.setEnabled(false);
                getImageFromGallery();
            }
        });

        registerBtn = (Button) root.findViewById(R.id.registration_register_btn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null) {

                    //Retrieve FCM Token
                    String fcmToken = FirebaseInstanceId.getInstance().getToken();
                    if(fcmToken == null) {
                        fcmToken = "";
                    }

                    if (user.getType() == User.UserType.FACEBOOK) {

                        String token = "";
                        if (user instanceof FBUser) {
                            token = ((FBUser) user).getFbToken();
                        }

                        //Fetch the uri from the FB PRofile object
                        String pictureUrl = "";
                        if(!TextUtils.isEmpty(user.getPicture_url())) {
                            pictureUrl = user.getPicture_url();
                        } else {
                            Profile profile = Profile.getCurrentProfile();
                            if(profile != null) {
                                pictureUrl = profile.getProfilePictureUri(200,200).toString();
                            }
                        }

                        showActivityProgress();
                        Call<FBRegistrationResponse> fbRegCall = RetrofitManager.getApiService(Constants.BASE_URL).registerFB(usernameET.getText().toString(), token, pictureUrl, fcmToken);
                        fbRegCall.enqueue(new Callback<FBRegistrationResponse>() {
                            @Override
                            public void onResponse(Response<FBRegistrationResponse> response, Retrofit retrofit) {
                                if (response != null && response.isSuccess()) {
                                    hideActivityProgress();
                                    //Registered
                                    saveUserAndGoHome(response.body().getUser());
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                hideActivityProgress();
                                showGeneralErroDialog();
                            }
                        });
                    } else {
                        showActivityProgress();
                        Call<RegistrationResponse> regCall = RetrofitManager.getApiService(Constants.BASE_URL).register(user.getEmail(), usernameET.getText().toString(), user.getPassword(), user.getPicture_url(), fcmToken);
                        regCall.enqueue(new Callback<RegistrationResponse>() {
                            @Override
                            public void onResponse(Response<RegistrationResponse> response, Retrofit retrofit) {
                                if (response != null && response.isSuccess()) {
                                    hideActivityProgress();
                                    //Registered
                                    saveUserAndGoHome(response.body().getUser());
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                hideActivityProgress();
                                showGeneralErroDialog();
                            }
                        });
                    }
                }
            }
        });


        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(user.getType() == User.UserType.FACEBOOK) {
            fbProfile = Profile.getCurrentProfile();
            if(fbProfile != null) {
                Uri pictureUri = fbProfile.getProfilePictureUri(200, 200);
                Picasso.with(getActivity()).load(pictureUri).into(pictBtn);
                usernameET.setText(fbProfile.getFirstName() + fbProfile.getLastName());
            }
        }

    }

    public void setUser(User user) {
        this.user = user;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void getImageFromGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
    }

    private void saveUser(User user) {
        Utils.saveUserToPreferences(getActivity(), user);
    }

    private void goHome() {
        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
        getActivity().finish();
    }

    private void saveUserAndGoHome(User user) {
        saveUser(user);
        goHome();
    }

    @Override
    public String getFragmentTag() {
        return null;
    }
}
