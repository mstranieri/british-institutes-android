package it.effedodici.british.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.effedodici.british.BaseActivity;
import it.effedodici.british.R;
import it.effedodici.british.models.Question;

/**
 * Created by mirkopinna on 17/11/16.
 */
public class LoseFragment extends GameFragment {

    private Question question;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.lose_fragment, container, false);
        //((TextView)root.findViewById(R.id.lost_lbl)).setText(getString(R.string.you_lost, question.getPoints()));
        root.findViewById(R.id.lose_continue_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof BaseActivity) {
                    requestClearBackStack(CategoriesFragment.TAG);
                }
            }
        });
        return root;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public String getFragmentTag() {
        return null;
    }
}
