package it.effedodici.british.rest;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by TT on 20.12.2015..
 */
public class RetrofitManager {

    private static ApiService apiService;
    private static ApiService apiService2;

    public static ApiService getApiService(String baseUrl){
        if(apiService == null){
            OkHttpClient client = new OkHttpClient();

            /*HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            if(BuildConfig.DEBUG) {
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            } else {
                interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
            }
            client.interceptors().add(interceptor);*/

            // Add default header to each request
            client.networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder().build();
                    return chain.proceed(request);
                }
            });

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build();

            apiService = retrofit.create(ApiService.class);
        }
        return apiService;
    }

    public static ApiService getApiService2(String baseUrl){
        if(apiService2 == null){
            /*okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

            // Add default header to each request
            client.networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder().build();
                    return chain.proceed(request);
                }
            });*/

            retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                    .addCallAdapterFactory(retrofit2.adapter.rxjava.RxJavaCallAdapterFactory.create())
                    .build();

            apiService2 = retrofit.create(ApiService.class);
        }
        return apiService2;
    }

    public static void invalidate() {
        apiService = null;
        apiService2 = null;
    }
}
