package it.effedodici.british.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.internal.Util;

import java.util.Arrays;

import it.effedodici.british.BaseActivity;
import it.effedodici.british.MainActivity;
import it.effedodici.british.R;
import it.effedodici.british.models.FBUser;
import it.effedodici.british.models.User;
import it.effedodici.british.rest.FBLoginResponse;
import it.effedodici.british.rest.LoginResponse;
import it.effedodici.british.rest.RegistrationResponse;
import it.effedodici.british.rest.RetrofitManager;
import it.effedodici.british.utils.Constants;
import it.effedodici.british.utils.SingleButtonDialog;
import it.effedodici.british.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by mirkopinna on 25/08/16.
 */
public class LoginFragment extends BaseFragment {

    private EditText emailET;
    private EditText passwordET;

    private Call<LoginResponse> loginCall;

    private LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.login_fragment, container, false);
        emailET = (EditText) root.findViewById(R.id.login_email_field);
        passwordET = (EditText) root.findViewById(R.id.login_password_field);

        //FB Login
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) root.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Use userid instead of token, userid is unique
                final String token = loginResult.getAccessToken().getUserId();
                if(!TextUtils.isEmpty(token)) {
                    Log.d("FB", "Success " + token);
                    fbLoginWithToken(token);
                }
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("FB", "Cancelled ");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("FB", "Error " + exception.toString());
            }
        });

        root.findViewById(R.id.login_register_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Request login
                final String email = emailET.getText().toString();
                String password = passwordET.getText().toString();

                showActivityProgress();
                loginCall = RetrofitManager.getApiService(Constants.BASE_URL).login(email, password);
                loginCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                        if(response != null && response.isSuccess()) {
                            hideActivityProgress();
                            LoginResponse logRes = response.body();
                            if(logRes.isLogged()) {
                                //User is logged in
                                User user = logRes.getUser();
                                Utils.saveUserToPreferences(getActivity(), user);

                                if(FirebaseInstanceId.getInstance().getToken() != null) {
                                    sendFCMTokenToServer(FirebaseInstanceId.getInstance().getToken());
                                } else {
                                    goHome();
                                }

                            } else {
                                //User is not logged-in show registration
                                if(getActivity() instanceof BaseActivity) {
                                    RegistrationFragment fragm = new RegistrationFragment();
                                    User user = new User();
                                    user.setEmail(emailET.getText().toString());
                                    user.setPassword(passwordET.getText().toString());
                                    fragm.setUser(user);
                                    ((BaseActivity) getActivity()).replaceFragmentRequest(fragm, null, true);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        hideActivityProgress();
                        showGeneralErroDialog();
                    }
                });
            }
        });

        return root;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*if(AccessToken.getCurrentAccessToken() != null) {
            String fbToken = AccessToken.getCurrentAccessToken().getToken();
            if(!TextUtils.isEmpty(fbToken)) {
                fbLoginWithToken(fbToken);
            }
        }*/
    }

    private void fbLoginWithToken(final String token) {
        showActivityProgress();
        Call<FBLoginResponse> fbLogCall = RetrofitManager.getApiService(Constants.BASE_URL).FBLogin(token);
        fbLogCall.enqueue(new Callback<FBLoginResponse>() {
            @Override
            public void onResponse(Response<FBLoginResponse> response, Retrofit retrofit) {
                if(response != null && response.isSuccess()) {
                    FBLoginResponse logRes = response.body();
                    if(logRes.isLogged()) {
                        //User is logged in
                        FBUser user = logRes.getUser();
                        Utils.saveUserToPreferences(getActivity(), user);
                        if(FirebaseInstanceId.getInstance().getToken() != null) {
                            sendFCMTokenToServer(FirebaseInstanceId.getInstance().getToken());
                        } else {
                            goHome();
                        }

                    } else {
                        //User is not logged-in show registration
                        if(getActivity() instanceof BaseActivity) {
                            RegistrationFragment fragm = new RegistrationFragment();
                            FBUser user = new FBUser();
                            user.setFbToken(token);
                            user.setType(User.UserType.FACEBOOK);
                            fragm.setUser(user);
                            ((BaseActivity) getActivity()).replaceFragmentRequest(fragm, null, true);
                        }
                    }
                }
                hideActivityProgress();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("FB", t.getLocalizedMessage());
                hideActivityProgress();
                showGeneralErroDialog();
            }
        });
    }

    private void goHome() {
        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
        getActivity().finish();
    }

    private void sendFCMTokenToServer(String token) {
        showActivityProgress();
        Call<RegistrationResponse> tokenCall = RetrofitManager.getApiService(Constants.BASE_URL).updateFCMToken(Utils.getUserFromDefaults(getActivity()).getId(), token);
        tokenCall.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Response<RegistrationResponse> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    Utils.saveUserToPreferences(getActivity(), response.body().getUser());
                }
                hideActivityProgress();
                goHome();
            }

            @Override
            public void onFailure(Throwable t) {
                hideActivityProgress();
                showGeneralErroDialog();
            }
        });
    }

    @Override
    public String getFragmentTag() {
        return "Login";
    }
}
