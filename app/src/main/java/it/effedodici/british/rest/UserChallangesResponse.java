package it.effedodici.british.rest;

import java.util.ArrayList;

import it.effedodici.british.models.Challange;

/**
 * Created by mirkopinna on 16/11/16.
 */
public class UserChallangesResponse {

    private ArrayList<Challange> challanges;

    public ArrayList<Challange> getChallanges() {
        return challanges;
    }

    public void setChallanges(ArrayList<Challange> challanges) {
        this.challanges = challanges;
    }
}
