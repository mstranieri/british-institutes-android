package it.effedodici.british.rest;

import it.effedodici.british.models.Challange;

/**
 * Created by mirkopinna on 15/11/16.
 */
public class AnswerResponse {

    private Challange challange;

    public Challange getChallange() {
        return challange;
    }

    public void setChallange(Challange challange) {
        this.challange = challange;
    }
}
